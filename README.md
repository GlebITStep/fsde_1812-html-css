# HTML & CSS 

## Links:

[Youtube Playlist](https://www.youtube.com/playlist?list=PLAP1LsqFV5B0rmkluholZeO7xrNDCuAAt)

[W3Schools](https://www.w3schools.com/)

[Emmet cheat sheet](https://docs.emmet.io/cheat-sheet/)

[DaFont](https://www.dafont.com/)

[GoogleFonts](https://fonts.google.com/)

[FlatIcon](https://www.flaticon.com)

[FontAwesome](https://fontawesome.com/)

[Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[Adobe Colors](https://color.adobe.com/explore)